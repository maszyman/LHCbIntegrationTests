###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
cmake_minimum_required(VERSION 3.18)

# get_project_version(project_name [output_variables])
#
# Find the preferred version of a project.
# the version is stored in the cache variable ${project_name}_version
# Also construct the command to be used to run (lb-run or ./run) and store it
# in the cache variable ${project_name}_run
function(get_project_version project_name)
  if(NOT "$ENV{slot}" STREQUAL ""
     AND NOT "$ENV{slot_build_id}" STREQUAL ""
     AND NOT "$ENV{LBENV_CURRENT_WORKSPACE}" STREQUAL ""
     AND EXISTS "$ENV{LBENV_CURRENT_WORKSPACE}/${project_name}")

    # we are in a nightly slot, so we can assume the .xenv files can be used without lb-run wrapping
    find_program(env_cmd NAMES xenv)
    if(NOT xenv_cmd)
      if(NOT EXISTS ${CMAKE_BINARY_DIR}/contrib/xenv)
        # Avoid interference from user environment
        unset(ENV{GIT_DIR})
        unset(ENV{GIT_WORK_TREE})
        execute_process(COMMAND git clone -b 1.0.0 https://gitlab.cern.ch/gaudi/xenv.git ${CMAKE_BINARY_DIR}/contrib/xenv)
      endif()
      # I'd like to generate the script with executable permission, but I only
      # found this workaround: https://stackoverflow.com/a/45515418
      # - write a temporary file
      file(WRITE "${CMAKE_BINARY_DIR}/xenv"
  "#!/usr/bin/env python
  from os.path import join, dirname, pardir
  import sys
  sys.path.append(join(dirname(__file__), pardir, 'python'))
  from xenv import main
  main()")
      # - copy it to the right place with right permissions
      file(COPY "${CMAKE_BINARY_DIR}/xenv" DESTINATION "${CMAKE_BINARY_DIR}/bin"
          FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
                            GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
      # - remove the temporary
      file(REMOVE "${CMAKE_BINARY_DIR}/xenv")
      # we have to copy the Python package to the bin directory to be able to run xenv
      # without setting PYTHONPATH
      file(COPY ${CMAKE_BINARY_DIR}/contrib/xenv/xenv DESTINATION "${CMAKE_BINARY_DIR}/python")

      set(xenv_cmd "${CMAKE_BINARY_DIR}/bin/xenv" CACHE FILEPATH "Path to xenv command" FORCE)

      install(PROGRAMS ${CMAKE_BINARY_DIR}/bin/xenv DESTINATION scripts)
      install(DIRECTORY ${CMAKE_BINARY_DIR}/contrib/xenv/xenv DESTINATION python
              FILES_MATCHING PATTERN "*.py" PATTERN "*.conf")
    endif()
    set(xenv_file $ENV{LBENV_CURRENT_WORKSPACE}/${project_name}/InstallArea/$ENV{BINARY_TAG}/${project_name}.xenv)
    if(NOT EXISTS ${xenv_file})
      message(FATAL_ERROR "${xenv_file}: file not found, needed to set the environment up in the nightly builds")
    endif()
    set(${project_name}_run ${xenv_cmd} -x ${xenv_file})
    message(STATUS "Using ${project_name} with ${xenv_file}")

  elseif(NOT DEFINED ${project_name}_version)
    get_filename_component(build ${CMAKE_BINARY_DIR} NAME)
    # get_filename_component(path ${CMAKE_BINARY_DIR} DIRECTORY)
    # get_filename_component(path ${path} DIRECTORY)
    # For some reason CMAKE_BINARY_DIR is a relative directory in the nightlies, so
    # the above does not work. Use LBENV_CURRENT_WORKSPACE instead:
    set(path "$ENV{LBENV_CURRENT_WORKSPACE}")
    set(local_run "${path}/${project_name}/${build}/run")
    message(STATUS "Checking for ${local_run}")
    if(TRY_BINARY_DIR_RUN AND EXISTS ${local_run})
      set(${project_name}_run ${local_run})
      set(version "")
    else()
      include(GetLbRunCmd)
      # get the list of versions of the project
      # (we need to manipulate the output of lb-run to get a space separated list
      # of version numbers)
      # Output is something like
      #   prod in /path/stack/DaVinci: x86_64_v2-centos7-gcc11-opt
      execute_process(COMMAND ${lbrun_cmd} --list ${project_name}
                      COMMAND cut "-d " "-f1"
                      COMMAND xargs
                      OUTPUT_VARIABLE versions
                      OUTPUT_STRIP_TRAILING_WHITESPACE
                      COMMAND_ERROR_IS_FATAL ANY)
      separate_arguments(versions)
      execute_process(COMMAND ${lbrun_cmd} --list ${project_name}
                      COMMAND cut "-d " "-f3"  # get third word
                      COMMAND sed "s/:$//"  # remove trailing colon
                      COMMAND xargs
                      OUTPUT_VARIABLE paths
                      OUTPUT_STRIP_TRAILING_WHITESPACE
                      COMMAND_ERROR_IS_FATAL ANY)
      separate_arguments(paths)
      # try "HEAD" and "prod" first
      list(FIND versions "HEAD" pos)
      if(NOT pos EQUAL -1)
        set(version "HEAD")
      else()
        list(FIND versions "prod" pos)
        if(NOT pos EQUAL -1)
          # empty version is incorrectly listed as prod, see LBCORE-1307
          set(version "")
        else()
          set(pos 0)
          list(GET versions ${pos} version)
        endif()
      endif()
      list(GET paths ${pos} path)
      if(NOT version STREQUAL "")
        set(${project_name}_run ${lbrun_cmd} ${project_name}/${version})
      else()
        # Project/ is not valid, see LBCORE-1307
        set(${project_name}_run ${lbrun_cmd} ${project_name})
      endif()
    endif()
    set(${project_name}_version "${version}" CACHE STRING "Version of ${project_name}")
    message(STATUS "Using ${project_name}/${version} from ${path} with ${${project_name}_run}")
  elseif(NOT DEFINED ${project_name}_run)
    set(${project_name}_run ${lbrun_cmd} ${project_name}/${${project_name}_version})
    message(STATUS "Using ${project_name}/${${project_name}_version} with ${${project_name}_run}")
  endif()

  set(${project_name}_run ${${project_name}_run} CACHE STRING "Env command for ${project_name}")

  if(ARGV1)
    set(${ARGV1} "${${project_name}_version}" PARENT_SCOPE)
  endif()
  if(ARGV2)
    set(${ARGV2} "${${project_name}_run}" PARENT_SCOPE)
  endif()
endfunction()
