LHCb Integration Tests Project
==============================

The LHCb Integration Tests project is a simple shell used to host tests meant to
check the integration between LHCb software projects.
